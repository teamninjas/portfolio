
var tabObject = (function($){

	var tab = {
		showTab : function (){
			e = this;
			var id = $(e).data('id');
			$('.tabs').removeClass('active');
			$(this).addClass('active');
			$('.list-info').removeClass('active-pane');
			$('.list-info').addClass('go-left-hide') ;
			$('#'+id).removeClass('go-left-hide').addClass('active-pane');
		}
	};

	return tab;
})(jQuery);

var postEvent = (function($){
	return {
		post:function(e){
			e.preventDefault();
			var form = $(this);
 			var data = postEvent.getFormData(form);
 			$.post('/',data,function(res){
 				alert('thank you for sending email to us .. we will send you a reply as soon as posible');
			});
		},
		getFormData: function($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};

		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });

		    return indexed_array;
		}
	}
})(jQuery);

$('.tabs').on('click',tabObject.showTab);
$('form').submit('click',postEvent.post);