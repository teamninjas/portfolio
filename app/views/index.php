<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>retrobyteninja</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/main.css">
        <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <iframe src="/img/banner.html" frameborder="0" class="anim"></iframe>
        <header>
            <div class="logo">
               </div>
            <div class="menu">  
                <ul>
                    <li><a href="#about">about us</a></li>
                    <li><a href="#portfolio">portfolio's</a></li>
                    <li><a href="#contact">contact us</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </header>

        <section class="banner-page">

            <div class="contains">
                <div class="device-image">
                    <span class="promo">
                        <span class="text-white">AWESOME GAMES</span>
                        <span class="skyblue"> & </span>
                        <span class="red">APPS</span></br>
                        <span class="skyblue">SOON</span>
                    </span>
                </div>
            </div>
        </section>
        <div class="divider"></div>
        
         <section class="tagline" id="about">
            <div class="contains">
                <span class="tagline">Retrobyte Ninja is a team of  Developers and designer devoted in creating clean and professional websites, and fun games for iOS and 
                    Android users <br> We are based from the Phillipines</span>

            </div>
        </section>
        
        <section class="pro-info" id="portfolio">
            <div class="contains">
                <span class="title_head">MEET THE TEAM</span>               
                <div class="list-ninjas">
                    <div class="list">
                        <div class="head">
                            <img src="<?php echo URL::to('/img/ninja1.png');?>" alt="">
                        </div>
                        <div class="desc">
                            <span>
                            The Creative Ninja
                            </span>
                        </div>
                    </div>
                    <div class="list">
                        <div class="head">
                            <img src="<?php echo URL::to('/img/ninja2.png');?>" alt="">
                        </div>
                        <div class="desc">
                            <span>
                            The Leader Ninja
                                
                            </span>
                        </div>
                    </div>
                    <div class="list">
                        <div class="head">
                            <img src="<?php echo URL::to('/img/ninja3.png');?>" alt="">
                        </div>
                        <div class="desc">
                            <span>
                            The Backend Ninja
                            </span>
                        </div>
                    </div>
                    <div class="list">
                        <div class="head">
                            <img src="<?php echo URL::to('/img/ninja4.png');?>" alt="">
                        </div>
                        <div class="desc">
                            <span>
                            The Super Masturace Ninja
                            </span>
                        </div>
                    </div>

                    <div style="clear:both"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </section>

        <section>
            
        </section>

    <!--     <section class="contact" id="contact">
            <div class="contains">
                <h1><div class="red">QUALITY WORK IS WHAT WE DO</div></h1>

                <form action="/" method="POST">
                    <label for="intro">Let's make something beautiful together</label>
                    <input type="text" name="name" placeholder="Name" class="text-input" required/>
                    <input type="email" class="text-input" name="email" placeholder="Email" />
                    <textarea name="message" placeholder="What we can do for you" id="" class="text-input" cols="30" rows="10" required></textarea>
                    <button class="btn submit">Send</button>
                </form>
            </div>
        </section> -->
        <footer>    
            <p>Copyright 2016. All rights reserved</p>
        </footer>
<!---->
<!--        <div class="custom-modal">-->
<!--        </div>-->
<!--        <div class="custom-modal-blanket">-->
<!--        </div>-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

      
    </body>
</html>
